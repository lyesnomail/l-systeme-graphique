
type command =
| Line of int
| Move of int
| Turn of int
| Store
| Restore

type position = {
  x: float;      (** position x *)
  y: float;      (** position y *)
  a: int;        (** angle of the direction *)
}


type 's word =
  | Symb of 's
  | Seq of 's word list
  | Branch of 's word

type 's rewrite_rules = 's -> 's word

type 's system = {
    axiom : 's word;
    rules : 's rewrite_rules;
    interp : 's -> command list }



type symbol = A|M|B|PLUS|MOINS|D|E|F|G|H|I|J|K|L|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|C|AUTRE

let char_to_symbol c = 
  match c with 
    'A'->Symb A 
  |'B'-> Symb B
  |'M'-> Symb M
  |'+'-> Symb PLUS
  |'-'-> Symb MOINS
  |'C'-> Symb C 
  | 'D' -> Symb D
  | 'E' -> Symb E
  | 'F' -> Symb F
  | 'G' -> Symb G
  | 'H' -> Symb H
  | 'I' -> Symb I
  | 'J' -> Symb J
  | 'K' -> Symb K
  | 'L' -> Symb L
  | 'N' -> Symb N
  | 'O' -> Symb O
  | 'P' -> Symb P
  | 'Q' -> Symb Q
  | 'R' -> Symb R
  | 'S' -> Symb S
  | 'T' -> Symb T
  | 'U' -> Symb U
  | 'V' -> Symb V
  | 'W' -> Symb W
  | 'X' -> Symb X
  | 'Y' -> Symb Y
  | 'Z' -> Symb Z
  |_-> Symb AUTRE


let rec list_to_axiom_lite1 l1  = 
  match l1 with 
    []-> []
  |h::rest ->
      if h ='[' then [Branch (Seq (list_to_axiom_lite1 rest))]@(list_to_axiom_lite1 (list_to_axiom_lite2 rest))
      else if h =']' then [] 
      else [char_to_symbol h]@(list_to_axiom_lite1 rest)
and list_to_axiom_lite2 l1 =
  match l1 with 
    []-> []
  |h:: rest ->
      if h ='[' then []
      else if h = ']' then rest 
      else list_to_axiom_lite2 rest
let rec string_to_char_list_v1 s n =
  if String.length s <= n then []
  else s.[n]::string_to_char_list_v1 s (n+1) 


let string_to_char_list s = string_to_char_list_v1 s 0 

let string_to_axiom mot  = let a =list_to_axiom_lite1 (string_to_char_list mot)
  in Seq (a)


let snow : symbol system =
  {
    axiom = string_to_axiom "A";
    rules =
      (function
       | A -> string_to_axiom "A+B+";
       | B -> string_to_axiom "-A-B";
       | s -> Symb s);
    interp =
      (function
       | A ->  [Line (10)]
       | B ->  [Line (10)]
       | PLUS ->  [Turn (90)]
       | MOINS ->  [Turn (-90)])
  }