**Identifiants:**
- **Nom** : Fezoui, **prénom** : Lyes, **identifiant sur GitLab** : @fezoui, **numéro d'étudiant** : 21965519
- **Nom** : Sadaoui, **prénom** : Mohamed Yanis, **identifiant sur GitLab** : @sadaoui, **numéro d'étudiant** : 21986699

**Fonctionnalités:**

Notre programme peut déterminer l'emplacement d'un fichier ".sys" ainsi que le nombre de substitution à faire
et ce de manière totalement interactive avec l'utilisateur. Ainsi ce dernier n'aura qu'à insérer les informations qui lui seront demandées ( emplacement 
du fichier ".sys" et nombre de substitution ) et le programme se chargera de générer et d'afficher une fenêtre contenant la forme graphique résultante
de l'interprétation du fichier ".sys" demandé.

Notons que pendant l'interaction avec l'utilisateur, l'insertion par celui-ci d'une instruction erronée 
( fichier qui n'est pas de type ".sys" ou bien fichier inexistant ) fera apparaitre un message d'erreur  .

**Compilation et exécution:**
 * Partie 1 Compilation :
    Afin de compiler notre projet : veuillez être sûr d'avoir installé la bibliothèque Graphics en premier lieu, puis une fois cela fait, insérez dans le terminal la commande  :
    **make** 
 * Partie 2 Exécution :
    Une fois le projet compilé, il vous suffira de l'exécuter avec la commande : **./main** 
    Puis de suivre les indications affiché par le programme :
      * 1-insertion d'un chemin menant vers un fichier ".sys" ( exemples/br1.sys par exemple )
      * 2-insertions du nombre d'iteration de substitution à partir de l'axiom contenu dans le fichier ".sys"  ( ce nombre est petit dans la plupart des  cas : 2,3,4,6..., 
        de plus un nombre trop grand chez certains fichier ".sys" provoquera une erreur dans le programe ). 
 
  * Partie 3 : S'en suit l'apparition d'une fenêtre graphique, et enfin l'affichage des formes géométriques généré par le programme.
* Notons également qu'une simple réexecution (**./main**) suffit pour afficher  d'autre formes à partir d'un autre fichier ".sys" (nul besoin de recompiler avec make).

**Découpage modulaire:**

Le programme est découpé en 5 modules :
- Les modules Turtle et Systems : Ils contiennent les types fournis de base pour le projet ( les types command et position pour turtle.ml et les types system, word, rewrite_rules pour systems.ml )
- Le module Main : Il contient les fonctions de substitution et de parcours indispensables à l'interprétation des commandes émises par les fichiers ".sys", 
                   mais aussi les fonctions de déplacement du curseur dans le graph, et surtout la fonction (call n) qui en lui faisant appelle permetera de mettre en lien les fonctions précédentes afin de dessiner un graphe
- Le module Main2 : Il contient la fonction main et son appel, ainsi que d'autres fonctions qui permettent  d'écrire dans un fichier draw.ml et 
                   de garantir la stabilité du programme face à certains choix que pourrait faire l'utilisateur (chemin vers "".sys" erronés par exemple)
- Le module Draw : Il sert principalement de passerelle entre un fichier ".sys" et le programme qui en premier lieu convertit le contenu d'un fichier ".sys"
                   puis le stocke dans le draw.ml ( en écrasant au passage les prcedantes données de draw.ml ), ainsi le programme n'aura plus qu'à utiliser le "nouveau" module Draw pour créer d'autre formes graphiques. 


**Organisation du travail:**

Nous avons commencé la phase de conception et d'implementation début décembre et nous nous sommes répartis les tâches comme suit :
- Lyes FEZOUI : les fonctions de substitution (rules et interp), ainsi que les fonctions permettant de faire bouger le curseur (position) sur le graphe.
- Mohamed Yanis SADAOUI : les fonctions permettant de lire depuis un fichier ".sys" et des creer à partir de celui-ci un nouveau module Draw ainsi que le main.

* la plupart des taches importentes furent finie aux environs du 25 décembre, nous nous sommes entretnue conjointement et régulièrement depuis cette date pour assembler nos travaux ainsi
qu'ajouter d'autres fonctions et ce afin de  rendre notre programme le plus fiable possible 

**Misc:**

Notons que le programme accepte uniquement les fichiers ".sys" ayant comme type symbole des lettres alphabétiques en majiscule [A-Z] et les signes  + et -   .