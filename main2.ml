let rec copy_lines ci list =
  try
    let x = input_line ci in
    if String.length x !=0 then
    if  x.[0]=='#' 
    then copy_lines ci list
    else copy_lines ci (list@[x])
    else 
    copy_lines ci (list@[x])
  with End_of_file -> list;;

let char_to_symbol1 c =
  match c with 
  |'A'->"A" 
  |'B'->"B"
  |'M'->"M"
  |'+'->"PLUS"
  |'-'-> "MOINS"
  |'C'->"C" 
  | 'D' ->  "D"
  | 'E' ->  "E"
  | 'F' ->  "F"
  | 'G' ->  "G"
  | 'H' ->  "H"
  | 'I' ->  "I"
  | 'J' ->  "J"
  | 'K' ->  "K"
  | 'L' ->  "L"
  | 'N' ->  "N"
  | 'O' ->  "O"
  | 'P' ->  "P"
  | 'Q' ->  "Q"
  | 'R' ->  "R"
  | 'S' ->  "S"
  | 'T' ->  "T"
  | 'U' ->  "U"
  | 'V' ->  "V"
  | 'W' ->  "W"
  | 'X' ->  "X"
  | 'Y' ->  "Y"
  | 'Z' ->  "Z"
  |_->"AUTRE";;

let cree_exemple c = output_string c "
type command =
| Line of int
| Move of int
| Turn of int
| Store
| Restore

type position = {
  x: float;      (** position x *)
  y: float;      (** position y *)
  a: int;        (** angle of the direction *)
}


type 's word =
  | Symb of 's
  | Seq of 's word list
  | Branch of 's word

type 's rewrite_rules = 's -> 's word

type 's system = {
    axiom : 's word;
    rules : 's rewrite_rules;
    interp : 's -> command list }
\n
\ntype symbol = A|M|B|PLUS|MOINS|D|E|F|G|H|I|J|K|L|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|C|AUTRE\n
let char_to_symbol c = 
  match c with 
    'A'->Symb A 
  |'B'-> Symb B
  |'M'-> Symb M
  |'+'-> Symb PLUS
  |'-'-> Symb MOINS
  |'C'-> Symb C 
  | 'D' -> Symb D
  | 'E' -> Symb E
  | 'F' -> Symb F
  | 'G' -> Symb G
  | 'H' -> Symb H
  | 'I' -> Symb I
  | 'J' -> Symb J
  | 'K' -> Symb K
  | 'L' -> Symb L
  | 'N' -> Symb N
  | 'O' -> Symb O
  | 'P' -> Symb P
  | 'Q' -> Symb Q
  | 'R' -> Symb R
  | 'S' -> Symb S
  | 'T' -> Symb T
  | 'U' -> Symb U
  | 'V' -> Symb V
  | 'W' -> Symb W
  | 'X' -> Symb X
  | 'Y' -> Symb Y
  | 'Z' -> Symb Z
  |_-> Symb AUTRE\n

let rec list_to_axiom_lite1 l1  = 
  match l1 with 
    []-> []
  |h::rest ->
      if h ='[' then [Branch (Seq (list_to_axiom_lite1 rest))]@(list_to_axiom_lite1 (list_to_axiom_lite2 rest))
      else if h =']' then [] 
      else [char_to_symbol h]@(list_to_axiom_lite1 rest)
and list_to_axiom_lite2 l1 =
  match l1 with 
    []-> []
  |h:: rest ->
      if h ='[' then []
      else if h = ']' then rest 
      else list_to_axiom_lite2 rest
let rec string_to_char_list_v1 s n =
  if String.length s <= n then []
  else s.[n]::string_to_char_list_v1 s (n+1) 


let string_to_char_list s = string_to_char_list_v1 s 0 

let string_to_axiom mot  = let a =list_to_axiom_lite1 (string_to_char_list mot)
  in Seq (a)\n

let snow : symbol system =
  {
    axiom = ";;

let list_axiom c (list : string list) = 
  let g = List.hd list  in  output_string c ("string_to_axiom \""^g^"\";\n");;


let rec list_rules_lite c1 l =
  match l with  
  |r :: rest ->if String.length r ==0 then output_string c1 "       | s -> Symb s);\n"
  else let red =String.sub r 2 ((String.length r)-2) in output_string c1 ("       | "^(char_to_symbol1 (r.[0]))^" -> "^"string_to_axiom \""^red^"\";\n");list_rules_lite c1 rest 
  | [] -> () ;;

  let list_rules c1 l =output_string c1 "    rules =\n      (function\n";list_rules_lite c1 l ;;



  let commende_to_string str =
    match String.get str 0 with
    'T'-> let nume = String.sub str 1 ((String.length str)-1) in " [Turn ("^nume^")]"
    |'M'->let nume = String.sub str 1 ((String.length str)-1) in " [Move ("^nume^")]"
    |'L'->let nume = String.sub str 1 ((String.length str)-1) in " [Line ("^nume^")]"
    | _ ->  " ";;
  let interpretation str =
    let a = String.sub str 2 ((String.length str)-2) in "       | "^(char_to_symbol1 (String.get str 0))^" -> "^(commende_to_string a)^"\n";;

    let interpretation1 str =
      let a = String.sub str 2 ((String.length str)-2) in "       | "^(char_to_symbol1 (String.get str 0))^" -> "^(commende_to_string a)^")\n  }";;

  let rec begin123 l =
    match l with 
    |r :: rest ->if String.length r ==0 then rest
    else begin123 rest
    | [] -> []
;;
    let rec list_interp_lite c1 l = 
      match l with 
        r::[]->let ca =interpretation1 r in output_string c1 ca
        |r::rest-> let ca =interpretation r in output_string c1 ca; list_interp_lite c1 rest
        |[]->()
        let list_interp c1 l =let t = begin123 l in  output_string c1 "    interp =\n      (function\n";list_interp_lite c1 t ;;

let string_to_list f =  let ci = open_in f in let l = copy_lines ci [] in close_in ci; l;;


let cree a = let c1 = open_out "draw.ml" in
let l = string_to_list a  in let t= List.tl (List.tl l) in cree_exemple c1 ;list_axiom c1 l;list_rules c1 t ;list_interp c1 t;close_out c1;;


let issystem_file f = 
  if String.length f <5 then false
  else
    let world = String.sub f ((String.length f) - 4) 4 
      in if world = ".sys" then true else false ;;

let file_existe1 f  = 
if (issystem_file f)= true && (Sys.file_exists f) = true then true 
else false;;
let is_int s =
  try ignore (int_of_string s); true
  with _ -> false;;
let red x s = let a = "./main2 "^x in 
    cree s ;
    Sys.command "ocamlc graphics.cma draw.ml turtle.ml systems.ml  -o main2 main.ml";
    Sys.command a;;

let err123 a = print_string "Insertion erroné\n\n";a;; 

let main()=
print_string "Inserez un chemin pour un fichier system (exemple : examples/br1.sys ou examples/koch1.sys ou autre emplacement ) \n";
let s = read_line() in
print_string "Inserez un nombre d'applications de substitutions (un nombre trop élevé pour un certain modèle choisi pourrait provoquer une erreur) \n";
let x = read_line() in 
let b = (is_int x) in 
let b1 = (file_existe1 s) in 
let b2 = (b1 && b) in  
  if (b2=true) then 
  red x s
  else 
  err123 5;;

main();;
    

