open Draw

let test_seq (fu :'s Draw.rewrite_rules) sy = 
  match fu sy with 
    Seq(s) -> s
  |_ -> [fu sy];;
        
let rec subtitution_lite_v1 (wo :'s Draw.word) (f :'s Draw.rewrite_rules) = 
  match wo with 
    Symb (sy)-> f sy
  | Seq (se)->Seq (subtitution_lite_v2 se f)
  | Branch (br)->Branch  (subtitution_lite_v1 br f )
and subtitution_lite_v2 list (f :'s Draw.rewrite_rules) = 
  match list with 
    []->[]
  |h::rest-> 
      match h with
        Symb (sy)-> (test_seq f sy)@(subtitution_lite_v2  rest f)
      | Seq (se)->(subtitution_lite_v2 se f)@(subtitution_lite_v2  rest f)
      | Branch (br)-> [Branch  (subtitution_lite_v1 br f )]@(subtitution_lite_v2  rest f);; 

let rec application_list (axiom :'s word)  (inter : 's -> command list) = 
  match axiom with 
    Symb(sy) -> inter sy
  |Branch (br) -> [Store]@(application_list br inter )@[Restore]
  | Seq (se) -> application_list_v1 se inter
                  
and application_list_v1 (wo1 :'s word list) inte  = 
  match wo1 with 
    []->[]
  |h::rest->
      match h with 
        Symb(sy) -> (inte sy)@(application_list_v1 rest inte)
      |Branch (br) -> [Store]@(application_list br inte )@[Restore]@(application_list_v1 rest inte) 
      |Seq (se)-> (application_list_v1 se inte)@(application_list_v1 rest inte) ;;


  


type t = float;;

let pi_h = acos 0. ;;
let degree_of_radian x = x *. 90. /. pi_h ;;
let radian_of_degree x = x *. pi_h /. 90. ;;
let degree d:float = d;;
let to_degree a:t = a;;
let radian r = degree_of_radian r;;
let to_radian a = radian_of_degree a;;
let move_position (p:position) (uniter : int) = 
  if sin(radian_of_degree (float_of_int (p.a))) = 1. 
  then {x = p.x ;y = p.y +. float_of_int(uniter) ; a=p.a}
  else if sin(radian_of_degree (float_of_int (p.a))) = -1.
  then {x = p.x ;y = p.y -. float_of_int(uniter) ; a=p.a}
  else if cos (radian_of_degree (float_of_int (p.a))) = 1. 
  then {x = p.x +. float_of_int(uniter) ;y = p.y  ; a=p.a}
  else if cos (radian_of_degree (float_of_int (p.a))) = -1. 
  then {x = p.x -. float_of_int(uniter) ;y = p.y  ; a=p.a}
  else let add_x = float_of_int(uniter)*.cos (radian_of_degree (float_of_int (p.a))) in 
    let add_y =float_of_int(uniter)*.sin(radian_of_degree (float_of_int (p.a)))
    in {x = p.x +. add_x ;y = p.y +. add_y ; a=p.a}
;;


let constructeur a1 b1 c1  :position ={x =float_of_int(a1) ; y =float_of_int(b1); a =c1} ;;


let tourner (p :position) angle = {x =p.x ; y =p.y ; a = p.a + angle};;


let rec application_turtel list1 list2 (curseur :position)  = 
  match list1 with 
    []-> 1
  |h::rest-> 
      match h with 
        Store -> application_turtel rest (curseur::list2) curseur
      |Restore -> application_turtel rest (List.tl list2) (List.hd list2) 
      |Turn(n)-> let newp = tourner curseur n in 
          application_turtel rest list2 newp
      | Move(n)->let newccoco = (move_position curseur n ) in
      let prepre1 = int_of_float(newccoco.x) in let prepre2 = int_of_float(newccoco.y) in 
      Graphics.moveto prepre1 prepre2; 
          application_turtel rest list2 newccoco 
      | Line (n)->let newccoco = (move_position curseur n ) in 
      let prepre1 = int_of_float(newccoco.x) in let prepre2 = int_of_float(newccoco.y) in 
       Graphics.lineto prepre1 prepre2;
          application_turtel rest list2 newccoco;;

let application_in_turtel list1 (curseur :position) = let prepre1 = int_of_float(curseur.x) in let prepre2 = int_of_float(curseur.y) in
 Graphics.moveto prepre1 prepre2 ; application_turtel list1 [] curseur;; 
 

 let rec with_rules (w: 's word)(f :'s Draw.rewrite_rules)(n: int)   = 
    if ( n > 0 ) then let t = subtitution_lite_v1 w f in  with_rules t f (n-1) else w 
 ;;

let with_interp (w: 's word) (f :'s rewrite_rules) (inter : 's -> command list) (n: int)  = 
  application_list ( with_rules w f n ) inter;;



let call n = 
    application_in_turtel ( with_interp ( snow.axiom ) ( snow.rules ) ( snow.interp ) (n) ) {x=640.0;y=360.0;a=90}  
;;


let close_after_event () =
    ignore(Graphics.wait_next_event[]);;

let ja x =
Graphics.open_graph " 1280x720";call x;close_after_event ();Graphics.close_graph();;

let main x = ja (int_of_string x) ;;


main Sys.argv.(1);;
